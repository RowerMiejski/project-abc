﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandItem : MonoBehaviour
{
    [SerializeField] int yeetSpeed = 20;
    [SerializeField] int yeetTorque = 20;

    public bool hasItem = false;
    public GameObject heldItem;
    public GameObject rightHand;
    PickupItem lastitem;
    bool hit = false;
    
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            yeet();
        }
    }
    public void additem(GameObject gameObject)
    {
        hasItem = true;
        heldItem = gameObject;
        Debug.Log(heldItem);
        gameObject.GetComponent<PickupItem>().changeLayer();
        moveItem();
    }
    public void dropHeldItem()
    {
        heldItem.transform.parent =null;

        lastitem = heldItem.GetComponent<PickupItem>();
        lastitem.isHolding = false;
        lastitem.changeLayer();
        heldItem = null;
        hasItem = false;

    }
    void moveItem()
    {
        heldItem.transform.SetParent(rightHand.transform);
        heldItem.transform.position = rightHand.transform.position;
    }
    public void yeet() {
        GameObject item = heldItem;
        Rigidbody2D rb = item.GetComponent<Rigidbody2D>();
        dropHeldItem();
        rb.AddForce(item.transform.right *yeetSpeed * (-1), ForceMode2D.Impulse); 
        rb.AddTorque(yeetTorque* (-1) * rb.mass , ForceMode2D.Impulse);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.gameObject.TryGetComponent(out PickupItem pickupItem))
        {

            pickupItem.gotHit(gameObject);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent(out PickupItem pickupItem))
        {
            pickupItem.stopHit();
        }
    }
}

