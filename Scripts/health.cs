﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class health : MonoBehaviour
{

    public float healthLevel = 100;
    public Slider HealthBar;
    public GameObject gientki;
    private SpriteRenderer spriteRenderer;
    bool initiated;


    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        
    }

    void Update()
    {
        HealthBar.value = healthLevel;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(healthLevel <= 0 && !initiated)
        {
            GameObject object1 = Instantiate(gientki, gameObject.transform.position - new Vector3(1.94f, -2.47f, 0f), transform.rotation);
            gameObject.active = false;
            initiated = true;
        }
        if (collision.gameObject.TryGetComponent(out bullet bullet))
        {
            healthLevel -= bullet.getDamage();
        }
    }
}
