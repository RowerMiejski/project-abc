﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shooting : MonoBehaviour
{
    [SerializeField] private int speed = 500;
    public GameObject bullet;
    PickupItem item;
    public Transform FirePoint;
    mouseFollow follow;
    Ammo ammo;
    private float hor;
    private float ver;
    private double time;
    double timer;
    public bool isInHand;

    void Start()
    {
        follow = GetComponent <mouseFollow>();
        item = gameObject.GetComponent<PickupItem>();
        ammo = GetComponent<Ammo>();
        double zm = item.cps;
        time = 1f / zm;
        timer = time;
    }



    void Update()
    {
        
        if (Input.GetMouseButton(0) && transform.parent != null && ammo.currentAmmo > 0)
        {
            
            timer -= Time.deltaTime;
            if (timer  <= 0f)
            {
                shoot();
                timer = time;
            }
        }
    }
    void shoot()
    {
        GameObject bulletObject = Instantiate(bullet, FirePoint.position, FirePoint.rotation);
        Rigidbody2D rbBullet = bulletObject.GetComponent<Rigidbody2D>();
        bullet bulletScr = bulletObject.GetComponent<bullet>();
        bulletScr.setDamage(item.damage);
        rbBullet.AddForce(FirePoint.up * speed, ForceMode2D.Impulse);
        ammo.currentAmmo -= 1;
    }
}
