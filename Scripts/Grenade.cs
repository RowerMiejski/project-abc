﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{

    public GameObject explosionAnimation;
    public bool explo;
    private bool countTime;
    private float timeToExplo = 3;
    private bool hasParent = false;

    void Start()
    {
        
    }

    void Update()
    {
        if(transform.parent != null)
        {
            hasParent = true;
        }
        timerUpdate();
        if (explo)
        {
            GameObject explosion = Instantiate(explosionAnimation, transform.position, transform.rotation);
            Destroy(gameObject);
            
        }
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (hasParent)
        {
            countTime = true;
        }
    }

    void timerUpdate()
    {
        if (countTime)
        {
            timeToExplo -= Time.deltaTime;
            if (timeToExplo <= 0)
            {
                explo = true;
            }
        }
    }
}
