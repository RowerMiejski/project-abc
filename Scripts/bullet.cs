﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{

    private float killTime = 5f;
    private float killTimeOnHit = 0.7f;
    private bool toKill = false;
    private float damage;
    
    void Start()
    {
        gameObject.layer = 9;
    }

    void Update()
    {
        killTime -= Time.deltaTime;
        if (killTime <= 0)
        {
            Destroy(gameObject);
        }
        if (toKill)
        {
            destroyOnHit();
        }
    }

   void OnCollisionEnter2D(Collision2D collision)
    {
        toKill = true;
        if (!(collision.gameObject.TryGetComponent(out health health))){
            damage = 0;
        }
    }
    void destroyOnHit()
    {
        killTimeOnHit -= Time.deltaTime;
        if (killTimeOnHit <= 0)
        {
            Destroy(gameObject);
        }
    }
    public void setDamage(float dmg)
    {
        damage = dmg;
    }
    public float getDamage()
    {
        return damage;
    }
}
