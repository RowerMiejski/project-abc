﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenManager : MonoBehaviour
{
    public GameObject item;
    public GameObject hand;
    private GameObject handItem;
    public Text ammo;


    void Start()
    {
        
    }


    void Update()
    {
        if (item.GetComponent<HandItem>().hasItem && hand.GetComponentInChildren<Ammo>() != null)
        {
            ammo.text = hand.GetComponentInChildren<Ammo>().currentAmmo + "/" + hand.GetComponentInChildren<Ammo>().maxAmmo;
        }
        else
        {
            ammo.text = "";
        }

    }
}
