﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouseFollow : MonoBehaviour
{
    public GameObject parent;
    public float angle;
    void Update()
    {
        if (transform.parent != null)
        {
            if (gameObject.transform.parent.gameObject == parent.gameObject)
            {
                Vector3 mousePos = Input.mousePosition;


                Vector3 objectPos = Camera.main.WorldToScreenPoint(transform.position);
                mousePos.x = mousePos.x - objectPos.x;
                mousePos.y = mousePos.y - objectPos.y;

                angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
                if (angle < 90 && angle > (-90))
                {
                    transform.rotation = Quaternion.Euler(new Vector3(0, 180, angle * (-1)));
                    angle = angle * (-1);

                }
                else
                {
                    transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 180));
                    angle = angle - 180;
                }
            }
        }
    }
}
