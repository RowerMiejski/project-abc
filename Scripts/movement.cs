﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviourPun
{

    [SerializeField] private float speed = 10; 
    [SerializeField] private float jump = 600;
    [SerializeField] private float gravity = 5;
    private Rigidbody2D rb;
    private int force;
    private float movementspeed;
    private bool isOnGround;
    public Animator anim;
    HandItem item;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = gravity;
        item = GetComponent<HandItem>();
    }


    void FixedUpdate()
    {
        if (photonView.IsMine)
        {
            move();

        }
    }
    void move()
    {
        var way = 0;
        if (Input.GetKey(KeyCode.D))
        {
            way = 1;
        }

        if (Input.GetKey(KeyCode.A))
        {
            way = -1;
        }


        if (Input.GetKey(KeyCode.Space))
        {
            if (isOnGround)
            {
                Vector2 movement = new Vector2(0, jump);
                rb.AddForce(movement);
                isOnGround = false;
            }
        }


        movementspeed = way * speed;
        anim.SetFloat("Speed", Mathf.Abs(movementspeed));
        anim.SetBool("Jump", !isOnGround);
        rb.velocity = new Vector2(movementspeed, rb.velocity.y);
    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.name == "podloga")
        {
            isOnGround = true;
        }
         

    }
}

