﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerNameInput : MonoBehaviour
    {

    [SerializeField] private TMP_InputField nameInputField = null;
    [SerializeField] private Button continueButton = null;
    private const string PlayerName = "Player1";


    // Start is called before the first frame update
    void Start()
    {
        SetUpInputField();
    }

    private void SetUpInputField()
    {
        if (!PlayerPrefs.HasKey(PlayerName))
        {
            return;
        }
        string defaultName = PlayerPrefs.GetString(PlayerName);
        nameInputField.text = defaultName;
        setPlayerName(defaultName);
    }

    public void setPlayerName(string name)
    {
        continueButton.interactable = !string.IsNullOrEmpty(nameInputField.text);
    }
    public void SavePlayerName()
    {
        string playerName = nameInputField.text;
        PhotonNetwork.NickName = playerName;
        PlayerPrefs.SetString(PlayerName, playerName);
    }
}