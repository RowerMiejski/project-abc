﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupItem : MonoBehaviour
{
    public int damage;
    private bool isNear = false;
    HandItem item;
    public bool isHolding = false;
    Collider2D collider;
    SpriteRenderer renderer;
    Rigidbody2D rb;
    public bool yeet;
    bool cooldown;
    public double cps = 300;
    private void Update()
    {
        if (isNear && Input.GetKey(KeyCode.E) && !isHolding && !cooldown)
        {
            if (item.hasItem)
            {
                item.dropHeldItem();
                item.additem(gameObject);
                isHolding = true;
            }
            else
            {
                item.additem(gameObject);
                isHolding = true;
            }

        }
    }
    private void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        collider = GetComponent<BoxCollider2D>();
    }
    public void changeLayer()
    {

        if (renderer.sortingOrder == 1) {
            renderer.sortingOrder = 0;
            switchBoxCollider();

        }
        else
        {
            renderer.sortingOrder = 1;
            switchBoxCollider();

        }


    }
    void switchBoxCollider()
    {
        if(collider.enabled == true)
        {
            collider.enabled = false;
            rb.bodyType = RigidbodyType2D.Kinematic;
        }
        else
        {
            collider.enabled = true;
            rb.bodyType = RigidbodyType2D.Dynamic;
            cooldown = true;
            Invoke("stopCooldown", 1);
        }
    }
    public void gotHit(GameObject gameObject)
    {
        item = gameObject.GetComponent<HandItem>();
        isNear = true;
    }
    public void stopHit()
    {
        isNear = false;
        item = null; 
    }
    void stopCooldown()
    {
        cooldown = false;
    }
    public double getCps()
    {
        return cps;
    }
}
