﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityKill : MonoBehaviour
{

    public float timeToKill = 2;

    void Start()
    {
        
    }

    void Update()
    {
        timeToKill -= Time.deltaTime;
        if (timeToKill <= 0)
        {
            Destroy(gameObject);
        }
    }
}
